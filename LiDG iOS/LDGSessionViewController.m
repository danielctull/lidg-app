//
//  LDGSessionViewController.m
//  LiDG
//
//  Created by Daniel Tull on 24.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGSessionViewController.h"

@interface LDGSessionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@property (nonatomic) NSProgress *imageProgress;
@end

@implementation LDGSessionViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.speakerNameLabel.text = self.session.speaker.name;
	self.sessionNameLabel.text = self.session.name;
	self.sessionDetailLabel.text = @"This is something truly long and wonderful. Yes, it really is something truly long and wonderful.";

	CGSize fittingSize = [self.tableHeaderView systemLayoutSizeFittingSize:self.view.bounds.size];
	self.tableHeaderView.bounds = CGRectMake(0.0f, 0.0f, fittingSize.width, fittingSize.height);

	CGSize size = self.avatarImageView.bounds.size;
	// Is this the only way to get the screen? The view isn't in the
	// hierarchy yet, so doesn't have a window, thus no screen.
	CGFloat scale = [UIScreen mainScreen].scale;
	size = CGSizeMake(size.width * scale, size.height * scale);

	self.imageProgress = [self.dataManager fetchImageForPerson:self.session.speaker
														  size:size
													completion:^(UIImage *image, NSError *error) {

		// If the progress is nil, that is because the cache has retreived the
		// image from memory instantly.
		NSTimeInterval duration = self.imageProgress == nil ? 0.0f : 1.0f/3.0f;
		[UIView transitionWithView:[self.avatarImageView superview]
						  duration:duration
						   options:UIViewAnimationOptionTransitionCrossDissolve
						animations:^{
							self.avatarImageView.image = image;
						} completion:nil];

		self.imageProgress = nil;
	}];
}

@end
