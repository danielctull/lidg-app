//
//  LDGLinkTableViewCell.m
//  LiDG
//
//  Created by Daniel Tull on 24.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGLinkTableViewCell.h"

@interface LDGLinkTableViewCell ()
@property (nonatomic) NSString *link;
@end

@implementation LDGLinkTableViewCell

- (NSURL *)URL {
	return [NSURL URLWithString:self.link];
}

@end

