//
//  LDGAboutViewController.m
//  LiDG
//
//  Created by Daniel Tull on 24.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGAboutViewController.h"
#import "LDGLinkTableViewCell.h"
#import <DCTWebBrowser/DCTWebBrowser.h>

@interface LDGAboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@end

@implementation LDGAboutViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	NSBundle *bundle = [NSBundle mainBundle];
	NSString *shortVersion = [bundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
	NSString *version = [bundle objectForInfoDictionaryKey:@"CFBundleVersion"];
	self.versionLabel.text = [NSString stringWithFormat:@"%@ (%@)", shortVersion, version];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setToolbarHidden:YES animated:animated];
}

- (IBAction)done:(id)sender {
	[self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

	if (![cell isKindOfClass:[LDGLinkTableViewCell class]]) return;

	LDGLinkTableViewCell *linkCell = (LDGLinkTableViewCell *)cell;

	DCTWebBrowser *browser = [DCTWebBrowser new];
	[browser loadRequest:[NSURLRequest requestWithURL:linkCell.URL]];
	[self.navigationController pushViewController:browser animated:YES];
}

@end
