//
//  LDGSessionTableViewCell.m
//  LiDG
//
//  Created by Daniel Tull on 24.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGSessionTableViewCell.h"

@interface LDGSessionTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *speakerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation LDGSessionTableViewCell

- (void)setSessionName:(NSString *)sessionName {
	self.sessionNameLabel.text = sessionName;
}

- (NSString *)sessionName {
	return self.sessionNameLabel.text;
}

- (void)setSpeakerName:(NSString *)speakerName {
	self.speakerNameLabel.text = speakerName;
}

- (NSString *)speakerName {
	return self.speakerNameLabel.text;
}

- (void)setDateString:(NSString *)dateString {
	self.dateLabel.text = dateString;
}

- (NSString *)dateString {
	return self.dateLabel.text;
}

@end
