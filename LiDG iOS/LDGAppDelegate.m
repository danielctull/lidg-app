//
//  LDGAppDelegate.m
//  LiDG iOS
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGAppDelegate.h"
#import <LiDG/LiDG.h>
#import "LDGEventsViewController.h"

@interface LDGAppDelegate ()

@property (nonatomic, strong) LDGDataManager *dataManager;

@end


@implementation LDGAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

	AFHTTPClient *HTTPClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://danieltull.co.uk/Clients/LiDG/"]];
	self.dataManager = [[LDGDataManager alloc] initWithHTTPClient:HTTPClient];

	UINavigationController *nav = (UINavigationController *)self.window.rootViewController;
	LDGEventsViewController *eventsViewController = (LDGEventsViewController *)nav.topViewController;
	eventsViewController.dataManager = self.dataManager;

	return YES;
}


- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
	[self.dataManager fetchEventsWithCompletion:^(NSArray *events, NSError *error) {
		if (events == nil) {
			completionHandler(UIBackgroundFetchResultFailed);
		}
		else {
			completionHandler(events.count > 0 ? UIBackgroundFetchResultNewData : UIBackgroundFetchResultNoData);
		}
	}];
}

@end
