//
//  LDGEventViewController.m
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGEventViewController.h"
#import "LDGSessionViewController.h"
#import "LDGSessionTableViewCell.h"
#import <DCTTableViewDataSources/DCTTableViewDataSources.h>
#import <DCTBanner/DCTBanner.h>
#import <MapKit/MapKit.h>

@interface LDGEventViewController ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) DCTTableViewDataSource *dataSource;
@end

@implementation LDGEventViewController

+ (NSDateFormatter *)dateFormatter {
	static NSDateFormatter *dateFormatter;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		dateFormatter = [NSDateFormatter new];
		dateFormatter.dateStyle = NSDateFormatterFullStyle;
		dateFormatter.timeStyle = NSDateFormatterNoStyle;
	});
	return dateFormatter;
}

+ (NSDateFormatter *)sessionDateFormatter {
	static NSDateFormatter *sessionDateFormatter;
	static dispatch_once_t sessionDateFormatterToken;
	dispatch_once(&sessionDateFormatterToken, ^{
		sessionDateFormatter = [NSDateFormatter new];
		sessionDateFormatter.dateStyle = NSDateFormatterNoStyle;
		sessionDateFormatter.timeStyle = NSDateFormatterShortStyle;
	});
	return sessionDateFormatter;
}

- (void)setEvent:(LDGEvent *)event {
	_event = event;
	self.title = event.name;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.dateLabel.text = [[[self class] dateFormatter] stringFromDate:self.event.date];

	dispatch_group_t group = dispatch_group_create();

	dispatch_group_enter(group);
	[self.dataManager fetchEvent:self.event completion:^(BOOL success, NSError *error) {

		if (!success) {
			DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleError title:@"Error" message:error.localizedDescription];
			[banner showFromNavigationBar:self.navigationController.navigationBar];
		}

		dispatch_group_leave(group);
	}];

	dispatch_group_enter(group);
	[self.dataManager fetchVenuesWithCompletion:^(NSArray *venues, NSError *error) {
		dispatch_group_leave(group);
	}];

	dispatch_group_notify(group, dispatch_get_main_queue(), ^{
		[self updateMap];
	});

	NSManagedObjectContext *context = self.dataManager.managedObjectContext;
	NSFetchRequest *sessionsRequest = [[NSFetchRequest alloc] initWithEntityName:[LDGSession entityName]];
	sessionsRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", LDGSessionRelationships.event, self.event];
	sessionsRequest.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:LDGSessionAttributes.startDate ascending:YES]];

	DCTFetchedResultsTableViewDataSource *sessionsDataSource = [[DCTFetchedResultsTableViewDataSource alloc] initWithManagedObjectContext:context
																															 fetchRequest:sessionsRequest];
	sessionsDataSource.cellReuseIdentifierHandler = ^(NSIndexPath *indexPath, id object) {
		return @"session";
	};
	//sessionsDataSource.sectionHeaderTitle = @"Sessions";

	self.dataSource = sessionsDataSource;
	self.tableView.dataSource = self.dataSource;
	self.dataSource.tableView = self.tableView;
}

- (void)updateMap {
	NSSet *sessions = self.event.sessions;
	NSUInteger count = [sessions count];

	if (count == 0) return;

	BOOL first = YES;

	CLLocationDegrees totalLongitude = 0.0;
	CLLocationDegrees totalLatitude = 0.0;

	CLLocationDegrees maxLongitude = 0.0f;
	CLLocationDegrees minLongitude = 0.0f;
	CLLocationDegrees maxLatitude = 0.0f;
	CLLocationDegrees minLatitude = 0.0f;

	for (LDGSession *session in sessions) {
		[self.mapView addAnnotation:session.venue];

		CLLocationDegrees longitude = session.venue.longitudeValue;
		CLLocationDegrees latitude = session.venue.latitudeValue;

		if (first) {
			minLatitude = latitude;
			maxLatitude = latitude;
			minLongitude = longitude;
			maxLongitude = longitude;
		} else {
			minLatitude = MIN(minLatitude, latitude);
			maxLatitude = MAX(maxLatitude, latitude);
			minLongitude = MIN(minLongitude, longitude);
			maxLongitude = MAX(maxLongitude, longitude);
		}

		totalLongitude += longitude;
		totalLatitude += latitude;

		first = NO;
	}


	CLLocationDegrees latitudeSpan = MAX(maxLatitude - minLatitude, 0.003);
	CLLocationDegrees longitudeSpan = MAX(maxLongitude - minLongitude, 0.003);
	CLLocationCoordinate2D center = CLLocationCoordinate2DMake(totalLatitude / count, totalLongitude / count);
	MKCoordinateSpan span = MKCoordinateSpanMake(latitudeSpan, longitudeSpan);
	self.mapView.region = MKCoordinateRegionMake(center, span);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

	id viewController = segue.destinationViewController;

	if ([viewController isKindOfClass:[LDGSessionViewController class]]) {
		LDGSessionViewController *sessionViewController = viewController;
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		sessionViewController.session = [self.dataSource objectAtIndexPath:indexPath];
		sessionViewController.dataManager = self.dataManager;		
	}
}

- (IBAction)action:(id)sender {
	NSArray *items = @[self.event.date, self.event.name];
	UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
	[self presentViewController:activityViewController animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	id object = [self.dataSource objectAtIndexPath:indexPath];

	if ([object isKindOfClass:[LDGSession class]] && [cell isKindOfClass:[LDGSessionTableViewCell class]]) {
		LDGSession *session = object;
		LDGSessionTableViewCell *sessionCell = (LDGSessionTableViewCell *)cell;
		sessionCell.sessionName = session.name;
		sessionCell.speakerName = session.speaker.name;
		sessionCell.dateString = [[[self class] sessionDateFormatter] stringFromDate:session.startDate];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	id object = [self.dataSource objectAtIndexPath:indexPath];

	if ([object isKindOfClass:[LDGSession class]]) return 60.0f;
	if ([object isKindOfClass:[LDGVenue class]]) return 200.0f;

	return 0.0f;
}

@end
