//
//  LDGEventsViewController.m
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGEventsViewController.h"
#import "LDGEventViewController.h"
#import <DCTTableViewDataSources/DCTTableViewDataSources.h>
#import <DCTBanner/DCTBanner.h>

@interface LDGEventsViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic) DCTTableViewDataSource *dataSource;
@end

@implementation LDGEventsViewController

+ (NSDateFormatter *)dateFormatter {
	static NSDateFormatter *dateFormatter;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		dateFormatter = [NSDateFormatter new];
		dateFormatter.dateStyle = NSDateFormatterFullStyle;
		dateFormatter.timeStyle = NSDateFormatterNoStyle;
	});
	return dateFormatter;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setupDataSource];

	[self.dataManager fetchEventsWithCompletion:^(NSArray *events, NSError *error) {

		if (events) return;

		DCTBanner *banner = [[DCTBanner alloc] initWithStyle:DCTBannerStyleError title:@"Error" message:error.localizedDescription];
		[banner showFromNavigationBar:self.navigationController.navigationBar];
	}];
	
	[self.dataManager fetchPeopleWithCompletion:nil];
}

- (IBAction)segmentedControlValueChanged:(id)sender {
	[self setupDataSource];
}

- (void)setupDataSource {
	NSManagedObjectContext *context = self.dataManager.managedObjectContext;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[LDGEvent entityName]];

	if (self.segmentedControl.selectedSegmentIndex == 0) {
		fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K < %@", LDGEventAttributes.date, [NSDate new]];
		fetchRequest.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:LDGEventAttributes.date ascending:NO]];
	} else {
		fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K > %@", LDGEventAttributes.date, [NSDate new]];
		fetchRequest.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:LDGEventAttributes.date ascending:YES]];
	}

	self.dataSource = [[DCTFetchedResultsTableViewDataSource alloc] initWithManagedObjectContext:context fetchRequest:fetchRequest];
	self.dataSource.cellReuseIdentifierHandler = ^(NSIndexPath *indexPath, id object) {
		return @"event";
	};

	self.tableView.dataSource = self.dataSource;
	self.dataSource.tableView = self.tableView;
	[self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

	id viewController = segue.destinationViewController;

	if ([viewController isKindOfClass:[LDGEventViewController class]]) {
		LDGEventViewController *eventViewController = viewController;
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		eventViewController.event = [self.dataSource objectAtIndexPath:indexPath];
		eventViewController.dataManager = self.dataManager;
	}
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	LDGEvent *event = [self.dataSource objectAtIndexPath:indexPath];
	cell.textLabel.text = event.name;
	cell.detailTextLabel.text = [[[self class] dateFormatter] stringFromDate:event.date];
}

@end
