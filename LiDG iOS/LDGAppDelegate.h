//
//  LDGAppDelegate.h
//  LiDG iOS
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
