//
//  LDGEventViewController.h
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LiDG/LiDG.h>

@interface LDGEventViewController : UITableViewController
@property (nonatomic) LDGEvent *event;
@property (nonatomic) LDGDataManager *dataManager;
@end
