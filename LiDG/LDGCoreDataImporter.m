//
//  LDGCoreDataImporter.m
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGCoreDataImporter.h"
#import <DCTManagedObjectSerialization/DCTManagedObjectSerialization.h>

@interface LDGCoreDataImporter ()
@property (nonatomic, readonly) DCTManagedObjectDeserializer *deserializer;
@end

@implementation LDGCoreDataImporter

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:NSManagedObjectContextDidSaveNotification
												  object:_deserializer.managedObjectContext];
}

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
	self = [self init];
	if (!self) return nil;
	_managedObjectContext = managedObjectContext;

	NSManagedObjectContext *importContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	importContext.persistentStoreCoordinator = managedObjectContext.persistentStoreCoordinator;

	_deserializer = [[DCTManagedObjectDeserializer alloc] initWithManagedObjectContext:importContext];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(managedObjectContextDidSaveNotification:)
												 name:NSManagedObjectContextDidSaveNotification
											   object:_deserializer.managedObjectContext];

	return self;
}

- (void)managedObjectContextDidSaveNotification:(NSNotification *)notification {

	NSSet *objects = [notification.userInfo valueForKey:NSUpdatedObjectsKey];
	NSSet *objectIDs = [objects valueForKey:@"objectID"];

	NSManagedObjectContext *mainContext = self.managedObjectContext;
	[mainContext performBlock:^{

		for (NSManagedObjectID *objectID in objectIDs) {
			NSManagedObject *object = [mainContext existingObjectWithID:objectID error:NULL];
			[object willAccessValueForKey:nil];
		}

		[mainContext mergeChangesFromContextDidSaveNotification:notification];
	}];
}

- (void)importArray:(NSArray *)array
		 entityName:(NSString *)entityName
		 completion:(void(^)(NSArray *managedObjects, NSError *error))completion {

	NSManagedObjectContext *mainContext = self.managedObjectContext;
	DCTManagedObjectDeserializer *deserializer = self.deserializer;
	NSManagedObjectContext *importContext = deserializer.managedObjectContext;
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:importContext];

	[importContext performBlock:^{

		NSArray *objects = [deserializer deserializeObjectsWithEntity:entity
															fromArray:array
											 existingObjectsPredicate:nil];

		NSError *error;
		BOOL success = [importContext save:&error];

		NSArray *objectIDs = [objects valueForKey:@"objectID"];

		[mainContext performBlock:^{

			if (!success) {
				completion(nil, error);
				return;
			}

			NSMutableArray *mainObjects = [[NSMutableArray alloc] initWithCapacity:objectIDs.count];
			[objectIDs enumerateObjectsUsingBlock:^(NSManagedObjectID *objectID, NSUInteger i, BOOL *stop) {
				id object = [mainContext objectWithID:objectID];
				[mainObjects addObject:object];
			}];

			completion([mainObjects copy], nil);
		}];
	}];
}

@end
