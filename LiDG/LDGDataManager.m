//
//  LDGDataManager.m
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGDataManager.h"
#import "LiDG.h"
#import "LDGCoreDataImporter.h"
#import "LDGPerson.h"
#import <DCTCoreDataStack/DCTCoreDataStack.h>
#import <DCTImageCache/DCTImageCache.h>
#import <DCTImageSizing/DCTImageSizing.h>

#import "AFImageRequestOperation.h"

@interface LDGDataManager ()
@property (nonatomic) DCTImageCache *imageCache;
@property (nonatomic) DCTCoreDataStack *coreDataStack;
@property (nonatomic) LDGCoreDataImporter *coreDataImporter;
@property (nonatomic) NSMutableDictionary *completionBlocks;
@end

@implementation LDGDataManager

- (id)initWithHTTPClient:(AFHTTPClient *)HTTPClient {

	self = [super init];
	if (!self) return nil;

	HTTPClient.requestSerializer = [AFJSONSerializer serializer];
	HTTPClient.responseSerializer = [AFJSONSerializer serializer];
	_HTTPClient = HTTPClient;

	NSURL *storeURL = [LiDG storeURL];
	NSURL *modelURL = [LiDG modelURL];
	_coreDataStack = [[DCTCoreDataStack alloc] initWithStoreURL:storeURL
													  storeType:NSSQLiteStoreType
												   storeOptions:nil
											 modelConfiguration:nil
													   modelURL:modelURL];

	_coreDataImporter = [[LDGCoreDataImporter alloc] initWithManagedObjectContext:_coreDataStack.managedObjectContext];
	_completionBlocks = [NSMutableDictionary new];

	_imageCache = [DCTImageCache imageCacheWithName:NSStringFromClass([self class])];
	__weak DCTImageCache *weakImageCache = _imageCache;
	_imageCache.imageFetcher = ^id<DCTImageCacheProcess>(DCTImageCacheAttributes *attributes, id<DCTImageCacheCompletion> completion) {

		CGSize size = attributes.size;

		// If it's size zero, we go and fetch the largest image - odd, I know.
		if (CGSizeEqualToSize(size, CGSizeZero)) {

			NSString *path = [NSString stringWithFormat:@"people/%@.png", attributes.key];
			NSURL *URL = [HTTPClient.baseURL URLByAppendingPathComponent:path];
			NSURLRequest *request = [NSURLRequest requestWithURL:URL];

			AFImageRequestOperation *operation = [AFImageRequestOperation imageRequestOperationWithRequest:request imageProcessingBlock:NULL success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
				[completion finishWithImage:image error:nil];
			} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
				[completion finishWithImage:nil error:error];
			}];

			[HTTPClient enqueueHTTPRequestOperation:operation];
			return operation;
		}

		// If we have a size then we request size zero (largest) and then resize it.
		NSMutableDictionary *attributeDictionary = [attributes.dictionary mutableCopy];
		[attributeDictionary setObject:[NSValue valueWithCGSize:CGSizeZero] forKey:DCTImageCacheAttributesSize];
		DCTImageCacheAttributes *zeroAttributes = [[DCTImageCacheAttributes alloc] initWithDictionary:attributeDictionary];
		return [weakImageCache fetchImageWithAttributes:zeroAttributes handler:^(UIImage *image, NSError *error) {
			image = [image dct_imageWithSize:attributes.size contentMode:UIViewContentModeScaleAspectFill];
			[completion finishWithImage:image error:error];
		}];
	};
	
	return self;
}

- (NSManagedObjectContext *)managedObjectContext {
	return self.coreDataStack.managedObjectContext;
}

- (NSProgress *)fetchImageForPerson:(LDGPerson *)person size:(CGSize)size completion:(void(^)(UIImage *image, NSError *error))completion {

	if (person.personID.length == 0) {
		completion(nil, nil);
		return nil;
	}

	DCTImageCacheAttributes *attributes = [[DCTImageCacheAttributes alloc] initWithDictionary: @{
																  DCTImageCacheAttributesKey : person.personID,
																 DCTImageCacheAttributesSize : [NSValue valueWithCGSize:size] }];
	id<DCTImageCacheProcess> process = [self.imageCache fetchImageWithAttributes:attributes handler:^(UIImage *image, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			completion(image, error);
		});
	}];

	if (!process) return nil;

	NSProgress *progress = [NSProgress new];
	progress.cancellationHandler = ^{
		[process cancel];
	};
	
	return progress;
}

- (NSProgress *)fetchPeopleWithCompletion:(void(^)(NSArray *people, NSError *error))completion {
	return [self fetchPath:@"people.json" entityName:[LDGPerson entityName] completion:completion];
}

- (NSProgress *)fetchEventsWithCompletion:(void (^)(NSArray *, NSError *))completion {
	return [self fetchPath:@"events.json" entityName:[LDGEvent entityName] completion:completion];
}

- (NSProgress *)fetchVenuesWithCompletion:(void(^)(NSArray *venues, NSError *error))completion {
	return [self fetchPath:@"venues.json" entityName:[LDGVenue entityName] completion:completion];
}

- (NSProgress *)fetchEvent:(LDGEvent *)event completion:(void(^)(BOOL success, NSError *error))completion {

	if (!completion) completion = ^(BOOL success, NSError *error){};
	NSString *path = [NSString stringWithFormat:@"events/%@.json", event.eventID];

	return [self fetchPath:path entityName:[LDGEvent entityName] completion:^(NSArray *objects, NSError *error) {
		completion(objects != nil, error);
	}];
}

- (NSProgress *)fetchPath:(NSString *)path entityName:(NSString *)entityName completion:(void(^)(NSArray *objects, NSError *error))completion {

	if (!completion) completion = ^(NSArray *objects, NSError *error){};

	NSMutableArray *existingBlocks = [self.completionBlocks objectForKey:path];

	// If there are any completion blocks, it means a request is in progress,
	// so we just add our completion block and await the outcome.
	if (existingBlocks.count > 0) {
		[existingBlocks addObject:completion];
		return nil;
	}

	// Create an array to hold all of the completion blocks for this request.
	existingBlocks = [NSMutableArray new];
	[self.completionBlocks setObject:existingBlocks forKey:path];
	[existingBlocks addObject:completion];

	// Set the completion handler to instead enumerate through all the
	// completion blocks in the dictionary.
	completion = ^(NSArray *objects, NSError *error) {

		for (void(^completionBlock)(NSArray *objects, NSError *error) in existingBlocks)
			completionBlock(objects, error);

		[self.completionBlocks removeObjectForKey:path];
	};

	NSURLRequest *request = [self.HTTPClient requestWithMethod:@"GET" URLString:path parameters:nil];
	AFHTTPRequestOperation *operation = [self.HTTPClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {

		if ([responseObject isKindOfClass:[NSDictionary class]])
			responseObject = @[responseObject];

		if (![responseObject isKindOfClass:[NSArray class]]) {
			completion(nil, nil);
			return;
		}

		[self.coreDataImporter importArray:responseObject entityName:entityName completion:^(NSArray *managedObjects, NSError *error) {
			completion(managedObjects, error);
		}];

	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			completion(nil, error);
		});
	}];
	[self.HTTPClient enqueueHTTPRequestOperation:operation];
	return operation.progress;
}

@end
