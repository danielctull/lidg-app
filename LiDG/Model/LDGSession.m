#import "LDGSession.h"


@interface LDGSession ()

// Private interface goes here.

@end


@implementation LDGSession

- (NSString *)description {

	return [NSString stringWithFormat:@"<%@: %p; id = %@; name = %@>",
			NSStringFromClass([self class]),
			self,
			self.sessionID,
			self.name];
}

@end
