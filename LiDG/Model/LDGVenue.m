#import "LDGVenue.h"


@interface LDGVenue ()

// Private interface goes here.

@end


@implementation LDGVenue

- (NSString *)description {

	return [NSString stringWithFormat:@"<%@: %p; id = %@; name = %@>",
			NSStringFromClass([self class]),
			self,
			self.venueID,
			self.name];
}

- (NSString *)title {
	return self.name;
}

- (CLLocationCoordinate2D)coordinate {
	return CLLocationCoordinate2DMake(self.latitudeValue, self.longitudeValue);
}

@end
