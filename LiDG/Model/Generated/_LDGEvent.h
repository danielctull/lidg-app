// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGEvent.h instead.

#import <CoreData/CoreData.h>


extern const struct LDGEventAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *eventID;
	__unsafe_unretained NSString *name;
} LDGEventAttributes;

extern const struct LDGEventRelationships {
	__unsafe_unretained NSString *sessions;
} LDGEventRelationships;

extern const struct LDGEventFetchedProperties {
} LDGEventFetchedProperties;

@class LDGSession;





@interface LDGEventID : NSManagedObjectID {}
@end

@interface _LDGEvent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LDGEventID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* eventID;



//- (BOOL)validateEventID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *sessions;

- (NSMutableSet*)sessionsSet;





@end

@interface _LDGEvent (CoreDataGeneratedAccessors)

- (void)addSessions:(NSSet*)value_;
- (void)removeSessions:(NSSet*)value_;
- (void)addSessionsObject:(LDGSession*)value_;
- (void)removeSessionsObject:(LDGSession*)value_;

@end

@interface _LDGEvent (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSString*)primitiveEventID;
- (void)setPrimitiveEventID:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveSessions;
- (void)setPrimitiveSessions:(NSMutableSet*)value;


@end
