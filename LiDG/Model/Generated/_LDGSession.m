// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGSession.m instead.

#import "_LDGSession.h"

const struct LDGSessionAttributes LDGSessionAttributes = {
	.endDate = @"endDate",
	.name = @"name",
	.sessionID = @"sessionID",
	.startDate = @"startDate",
};

const struct LDGSessionRelationships LDGSessionRelationships = {
	.event = @"event",
	.speaker = @"speaker",
	.venue = @"venue",
};

const struct LDGSessionFetchedProperties LDGSessionFetchedProperties = {
};

@implementation LDGSessionID
@end

@implementation _LDGSession

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LDGSession" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LDGSession";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LDGSession" inManagedObjectContext:moc_];
}

- (LDGSessionID*)objectID {
	return (LDGSessionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic endDate;






@dynamic name;






@dynamic sessionID;






@dynamic startDate;






@dynamic event;

	

@dynamic speaker;

	

@dynamic venue;

	






@end
