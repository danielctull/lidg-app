// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGPerson.h instead.

#import <CoreData/CoreData.h>


extern const struct LDGPersonAttributes {
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *github;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *personID;
	__unsafe_unretained NSString *twitter;
	__unsafe_unretained NSString *webpageURL;
} LDGPersonAttributes;

extern const struct LDGPersonRelationships {
	__unsafe_unretained NSString *sessions;
} LDGPersonRelationships;

extern const struct LDGPersonFetchedProperties {
} LDGPersonFetchedProperties;

@class LDGSession;






@class NSObject;

@interface LDGPersonID : NSManagedObjectID {}
@end

@interface _LDGPerson : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LDGPersonID*)objectID;





@property (nonatomic, strong) NSString* email;



//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* github;



//- (BOOL)validateGithub:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* personID;



//- (BOOL)validatePersonID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* twitter;



//- (BOOL)validateTwitter:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id webpageURL;



//- (BOOL)validateWebpageURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *sessions;

- (NSMutableSet*)sessionsSet;





@end

@interface _LDGPerson (CoreDataGeneratedAccessors)

- (void)addSessions:(NSSet*)value_;
- (void)removeSessions:(NSSet*)value_;
- (void)addSessionsObject:(LDGSession*)value_;
- (void)removeSessionsObject:(LDGSession*)value_;

@end

@interface _LDGPerson (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;




- (NSString*)primitiveGithub;
- (void)setPrimitiveGithub:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitivePersonID;
- (void)setPrimitivePersonID:(NSString*)value;




- (NSString*)primitiveTwitter;
- (void)setPrimitiveTwitter:(NSString*)value;




- (id)primitiveWebpageURL;
- (void)setPrimitiveWebpageURL:(id)value;





- (NSMutableSet*)primitiveSessions;
- (void)setPrimitiveSessions:(NSMutableSet*)value;


@end
