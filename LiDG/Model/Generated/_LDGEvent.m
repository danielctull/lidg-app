// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGEvent.m instead.

#import "_LDGEvent.h"

const struct LDGEventAttributes LDGEventAttributes = {
	.date = @"date",
	.eventID = @"eventID",
	.name = @"name",
};

const struct LDGEventRelationships LDGEventRelationships = {
	.sessions = @"sessions",
};

const struct LDGEventFetchedProperties LDGEventFetchedProperties = {
};

@implementation LDGEventID
@end

@implementation _LDGEvent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LDGEvent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LDGEvent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LDGEvent" inManagedObjectContext:moc_];
}

- (LDGEventID*)objectID {
	return (LDGEventID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic date;






@dynamic eventID;






@dynamic name;






@dynamic sessions;

	
- (NSMutableSet*)sessionsSet {
	[self willAccessValueForKey:@"sessions"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sessions"];
  
	[self didAccessValueForKey:@"sessions"];
	return result;
}
	






@end
