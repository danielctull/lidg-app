// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGVenue.m instead.

#import "_LDGVenue.h"

const struct LDGVenueAttributes LDGVenueAttributes = {
	.imageURL = @"imageURL",
	.latitude = @"latitude",
	.longitude = @"longitude",
	.name = @"name",
	.venueID = @"venueID",
	.webURL = @"webURL",
};

const struct LDGVenueRelationships LDGVenueRelationships = {
	.sessions = @"sessions",
};

const struct LDGVenueFetchedProperties LDGVenueFetchedProperties = {
};

@implementation LDGVenueID
@end

@implementation _LDGVenue

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LDGVenue" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LDGVenue";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LDGVenue" inManagedObjectContext:moc_];
}

- (LDGVenueID*)objectID {
	return (LDGVenueID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic imageURL;






@dynamic latitude;



- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithDouble:value_]];
}





@dynamic longitude;



- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:[NSNumber numberWithDouble:value_]];
}





@dynamic name;






@dynamic venueID;






@dynamic webURL;






@dynamic sessions;

	
- (NSMutableSet*)sessionsSet {
	[self willAccessValueForKey:@"sessions"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sessions"];
  
	[self didAccessValueForKey:@"sessions"];
	return result;
}
	






@end
