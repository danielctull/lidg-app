// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGVenue.h instead.

#import <CoreData/CoreData.h>


extern const struct LDGVenueAttributes {
	__unsafe_unretained NSString *imageURL;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *venueID;
	__unsafe_unretained NSString *webURL;
} LDGVenueAttributes;

extern const struct LDGVenueRelationships {
	__unsafe_unretained NSString *sessions;
} LDGVenueRelationships;

extern const struct LDGVenueFetchedProperties {
} LDGVenueFetchedProperties;

@class LDGSession;

@class NSObject;




@class NSObject;

@interface LDGVenueID : NSManagedObjectID {}
@end

@interface _LDGVenue : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LDGVenueID*)objectID;





@property (nonatomic, strong) id imageURL;



//- (BOOL)validateImageURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* latitude;



@property double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* longitude;



@property double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* venueID;



//- (BOOL)validateVenueID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id webURL;



//- (BOOL)validateWebURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *sessions;

- (NSMutableSet*)sessionsSet;





@end

@interface _LDGVenue (CoreDataGeneratedAccessors)

- (void)addSessions:(NSSet*)value_;
- (void)removeSessions:(NSSet*)value_;
- (void)addSessionsObject:(LDGSession*)value_;
- (void)removeSessionsObject:(LDGSession*)value_;

@end

@interface _LDGVenue (CoreDataGeneratedPrimitiveAccessors)


- (id)primitiveImageURL;
- (void)setPrimitiveImageURL:(id)value;




- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;




- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveVenueID;
- (void)setPrimitiveVenueID:(NSString*)value;




- (id)primitiveWebURL;
- (void)setPrimitiveWebURL:(id)value;





- (NSMutableSet*)primitiveSessions;
- (void)setPrimitiveSessions:(NSMutableSet*)value;


@end
