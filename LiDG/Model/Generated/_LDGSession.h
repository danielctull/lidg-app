// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGSession.h instead.

#import <CoreData/CoreData.h>


extern const struct LDGSessionAttributes {
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *sessionID;
	__unsafe_unretained NSString *startDate;
} LDGSessionAttributes;

extern const struct LDGSessionRelationships {
	__unsafe_unretained NSString *event;
	__unsafe_unretained NSString *speaker;
	__unsafe_unretained NSString *venue;
} LDGSessionRelationships;

extern const struct LDGSessionFetchedProperties {
} LDGSessionFetchedProperties;

@class LDGEvent;
@class LDGPerson;
@class LDGVenue;






@interface LDGSessionID : NSManagedObjectID {}
@end

@interface _LDGSession : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LDGSessionID*)objectID;





@property (nonatomic, strong) NSDate* endDate;



//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* sessionID;



//- (BOOL)validateSessionID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* startDate;



//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) LDGEvent *event;

//- (BOOL)validateEvent:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) LDGPerson *speaker;

//- (BOOL)validateSpeaker:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) LDGVenue *venue;

//- (BOOL)validateVenue:(id*)value_ error:(NSError**)error_;





@end

@interface _LDGSession (CoreDataGeneratedAccessors)

@end

@interface _LDGSession (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveSessionID;
- (void)setPrimitiveSessionID:(NSString*)value;




- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;





- (LDGEvent*)primitiveEvent;
- (void)setPrimitiveEvent:(LDGEvent*)value;



- (LDGPerson*)primitiveSpeaker;
- (void)setPrimitiveSpeaker:(LDGPerson*)value;



- (LDGVenue*)primitiveVenue;
- (void)setPrimitiveVenue:(LDGVenue*)value;


@end
