// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LDGPerson.m instead.

#import "_LDGPerson.h"

const struct LDGPersonAttributes LDGPersonAttributes = {
	.email = @"email",
	.github = @"github",
	.name = @"name",
	.personID = @"personID",
	.twitter = @"twitter",
	.webpageURL = @"webpageURL",
};

const struct LDGPersonRelationships LDGPersonRelationships = {
	.sessions = @"sessions",
};

const struct LDGPersonFetchedProperties LDGPersonFetchedProperties = {
};

@implementation LDGPersonID
@end

@implementation _LDGPerson

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LDGPerson" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LDGPerson";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LDGPerson" inManagedObjectContext:moc_];
}

- (LDGPersonID*)objectID {
	return (LDGPersonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic email;






@dynamic github;






@dynamic name;






@dynamic personID;






@dynamic twitter;






@dynamic webpageURL;






@dynamic sessions;

	
- (NSMutableSet*)sessionsSet {
	[self willAccessValueForKey:@"sessions"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sessions"];
  
	[self didAccessValueForKey:@"sessions"];
	return result;
}
	






@end
