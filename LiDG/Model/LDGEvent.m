#import "LDGEvent.h"


@interface LDGEvent ()

// Private interface goes here.

@end


@implementation LDGEvent

- (NSString *)description {

	return [NSString stringWithFormat:@"<%@: %p; id = %@; name = %@>",
			NSStringFromClass([self class]),
			self,
			self.eventID,
			self.name];
}

@end
