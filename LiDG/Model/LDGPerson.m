#import "LDGPerson.h"


@interface LDGPerson ()

// Private interface goes here.

@end


@implementation LDGPerson

- (NSString *)description {

	return [NSString stringWithFormat:@"<%@: %p; id = %@; name = %@>",
			NSStringFromClass([self class]),
			self,
			self.personID,
			self.name];
}

@end
