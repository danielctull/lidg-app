#import "_LDGVenue.h"
#import <MapKit/MapKit.h>

@interface LDGVenue : _LDGVenue <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@end
