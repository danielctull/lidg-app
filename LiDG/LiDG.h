//
//  LiDG.h
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDGDataManager.h"
#import "LDGEvent.h"
#import "LDGPerson.h"
#import "LDGSession.h"
#import "LDGVenue.h"

@interface LiDG : NSObject

+ (NSBundle *)bundle;
+ (NSURL *)modelURL;
+ (NSURL *)storeURL;

@end
