//
//  LDGEpochDateTransformer.m
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import "LDGEpochDateTransformer.h"

@implementation LDGEpochDateTransformer

+ (Class)transformedValueClass {
	return [NSDate class];
}

+ (BOOL)allowsReverseTransformation {
	return YES;
}

- (id)transformedValue:(NSNumber *)value {
	if (!value) return nil;
	if ([value isKindOfClass:[NSNull class]]) return nil;
	return [[NSDate alloc] initWithTimeIntervalSince1970:[value doubleValue]];
}

- (id)reverseTransformedValue:(NSDate *)value {
	return @([value timeIntervalSince1970]);
}

@end
