//
//  LDGDataManager.h
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "LDGEvent.h"
#import "LDGPerson.h"

@interface LDGDataManager : NSObject

- (id)initWithHTTPClient:(AFHTTPClient *)HTTPClient;
@property (nonatomic, readonly) AFHTTPClient *HTTPClient;
@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

- (NSProgress *)fetchPeopleWithCompletion:(void(^)(NSArray *people, NSError *error))completion;
- (NSProgress *)fetchEventsWithCompletion:(void(^)(NSArray *events, NSError *error))completion;
- (NSProgress *)fetchVenuesWithCompletion:(void(^)(NSArray *venues, NSError *error))completion;
- (NSProgress *)fetchEvent:(LDGEvent *)event completion:(void(^)(BOOL success, NSError *error))completion;

- (NSProgress *)fetchImageForPerson:(LDGPerson *)person size:(CGSize)size completion:(void(^)(UIImage *image, NSError *error))completion;

@end
