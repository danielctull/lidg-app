//
//  LDGCoreDataImporter.h
//  LiDG
//
//  Created by Daniel Tull on 23.08.2013.
//  Copyright (c) 2013 Daniel Tull. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface LDGCoreDataImporter : NSObject

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

- (void)importArray:(NSArray *)array
		 entityName:(NSString *)entityName
		 completion:(void(^)(NSArray *managedObjects, NSError *error))completion;

@end
